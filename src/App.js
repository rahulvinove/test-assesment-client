import { Outlet, NavLink } from "react-router-dom";
import "./App.css"
export default function App() {
  return (
    <div>
      <nav
        style={{
          borderBottom: "solid 1px",
          paddingBottom: "1rem",
        }}
      >
        <NavLink to="/" activeClassName="selected">home</NavLink> |{" "}
        <NavLink to="/upload" activeClassName="selected">upload</NavLink> |{" "}
        <NavLink to="/users" activeClassName="selected">users</NavLink>
      </nav>
      <Outlet />
    </div>
  );
}
