import React, {useCallback} from 'react'
import {useDropzone} from 'react-dropzone'

export default function Upload() {

  const onDrop = useCallback(acceptedFiles => {
    // Do something with the files
    console.log("acceptedFiles", acceptedFiles)

    var formdata = new FormData();
    formdata.append("export", acceptedFiles[0], acceptedFiles[0].name);

    var requestOptions = {
      method: 'POST',
      body: formdata,
      redirect: 'follow'
    };

    fetch("http://localhost:8080/api/users", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
    
  }, [])
  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

  return (
    <div style={{display: "flex", padding: "100px" }}>
      <div {...getRootProps()} style={{ margin: "100px auto", height: "100px", width: "400px", border: "solid 1px", cursor: "pointer", textAlign: "center"}}>
        <input {...getInputProps()} />
        {
          isDragActive ?
            <p>Drop the files here ...</p> :
            <p>Drag file here to upload</p>
        }
      </div>
      <div style={{marginLeft: "48px"}}>
        <h3>Csv upload file</h3>
        <div style={{color: "green", border: "solid 1px", padding: "24px"}}>
          <p>name, email, phones</p>
          <p>name, email, phones</p>
          <p>name, email, phones</p>
        </div>
      </div>  
    </div>

    
  )
}
