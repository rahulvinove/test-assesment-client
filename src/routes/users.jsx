
import { useState, useEffect, createRef } from "react";
import TableComponent from "../components/TableComponent"

export default function Users() {
    const [users, setUser ] = useState([]);
    useEffect(()=>{
      fetch('http://localhost:8080/api/users')
      .then((response) => response.json())
      .then((data) => {
        console.log(data)
        setUser(data)
      });
    },[])
    return (
      <main style={{ padding: "1rem 0" }}>
        <TableComponent
          rows={users}
          cols={[
            {
              key: "id",
              label: "ID",
            },
            {
              key: "name",
              label: "Name",
            },
            {
              key: "email",
              label: "Email",
            },
            {
              key: "phones",
              label: "Phones",
            },
            {
              key: "createdAt",
              label: "Created Date",
            },
          ]}
        />
      </main>
    );
  }