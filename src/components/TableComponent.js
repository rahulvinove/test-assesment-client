import { useState, useRef, useEffect } from "react"
import "./style.css"

const PhoneComponent = ({data, setPosition}) =>{
  const phones = data.split("|")
  const handleClick = (e) =>{
    setPosition({
      data: phones,
      anchorPosition: {
        left: e.pageX,
        top: e.pageY
      }
    })
  }
  return (
    <>
      <u style={{cursor: "pointer", color: "blue"}} onClick={handleClick}>[{phones.join(", ")}]</u>
    </>
  )
}

const TableComponent = ({ rows, cols }) => {
  const [postion, setPosition] = useState(null)
  const ref = useRef();

  const checkIfClickedOutside = (e) => {
    if (postion && ref.current && !ref.current.contains(e.target)) {
      setPosition(null);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", checkIfClickedOutside);
    return () => {
      // Cleanup the event listener
      document.removeEventListener("mousedown", checkIfClickedOutside);
    };
  }, [postion]);
  
  return (
    <div
      style={{ padding: "100px"}}
      
    >
      <table >
        <thead >
          <tr>
            {cols.map((column, i) => (
              <th
                key={column.name}
              >
                {column.label}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {rows.map((row, i) => (
            <tr key={row.id} >
              {cols.map((col) => (
                <td>
                  { col.key === "phones" ? <PhoneComponent data={row[col.key]} setPosition={setPosition}/> : row[col.key]}
                </td>
              ))}
            </tr>
          ))}
          {!rows.length && (
            <tr>
              <td
                colSpan={cols.length}
                
              >
                No data found
              </td>
            </tr>
          )}
        </tbody>
      </table>
      {postion !== null && (
        <div 
          className="wrapper" 
          ref={ref} 
          style={{
            position: "absolute", 
            left: postion.anchorPosition.left, 
            top: postion.anchorPosition.top + 10, 
            border: "solid 1px", 
            listStyle: "none", 
            backgroundColor: "lightgray", 
            padding: "10px"
          }}
        >
        {postion.data.map((phone)=><li style={{width: "80px", height: "40px"}}><a href={`tel: ${phone}`}>{phone}</a></li>)}
      </div>
      )}
    </div>
  );
};
export default TableComponent