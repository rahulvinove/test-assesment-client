import React from 'react';
import ReactDOM from 'react-dom/client';

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import App from "./App";
import Upload from "./routes/upload";
import Users from "./routes/users";
import "./index.css"
const root = ReactDOM.createRoot(document.getElementById('root'));


root.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App />}>
        <Route path="upload" element={<Upload />} />
        <Route path="users" element={<Users />} />
      </Route>
    </Routes>
  </BrowserRouter>
);

